import { Request, Response } from 'express';
import { UserService } from '../services/CreateUserService';

export class UserController {
    async create(request: Request, response: Response): Promise<Response> {
        const { name, email, password } = request.body;

        const userService = new UserService();

        try {
            const user = await userService.create({ name, email, password });
            return response.status(201).json(user);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async update(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;
        const { name, email, password } = request.body;

        const userService = new UserService();

        try {
            const user = await userService.update(id, { name, email, password });
            return response.status(200).json(user);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async list(request: Request, response: Response): Promise<Response> {
        const userService = new UserService();

        try {
            const users = await userService.list();
            return response.status(200).json(users);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async delete(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;

        const userService = new UserService();

        try {
            await userService.delete(id);
            return response.status(204).send();
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }
}

import { Request, Response } from 'express';
import { SubTypeService } from '../services/CreateSubTypeService';

export class SubTypeController {
    async create(request: Request, response: Response): Promise<Response> {
        const { name, typeId } = request.body;

        const subTypeService = new SubTypeService();

        try {
            const subType = await subTypeService.create({ name, typeId });
            return response.status(201).json(subType);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async update(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;
        const { name, typeId } = request.body;

        const subTypeService = new SubTypeService();

        try {
            const subType = await subTypeService.update(id, { name, typeId });
            return response.status(200).json(subType);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async list(request: Request, response: Response): Promise<Response> {
        const subTypeService = new SubTypeService();

        try {
            const subTypes = await subTypeService.list();
            return response.status(200).json(subTypes);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async delete(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;

        const subTypeService = new SubTypeService();

        try {
            await subTypeService.delete(id);
            return response.status(204).send();
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }
}

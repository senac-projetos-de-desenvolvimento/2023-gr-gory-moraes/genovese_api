import { Request, Response } from 'express';
import { TypeService } from '../services/CreateTypeService';

export class TypeController {
    async create(request: Request, response: Response): Promise<Response> {
        const { name, subTypeId, categoryIds } = request.body;

        const typeService = new TypeService();

        try {
            const type = await typeService.create({ name, subTypeId, categoryIds });
            return response.status(201).json(type);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async update(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;
        const { name, subTypeId, categoryIds } = request.body;

        const typeService = new TypeService();

        try {
            const type = await typeService.update(id, { name, subTypeId, categoryIds });
            return response.status(200).json(type);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async list(request: Request, response: Response): Promise<Response> {
        const typeService = new TypeService();

        try {
            const types = await typeService.list();
            return response.status(200).json(types);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async delete(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;

        const typeService = new TypeService();

        try {
            await typeService.delete(id);
            return response.status(204).send();
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }
}

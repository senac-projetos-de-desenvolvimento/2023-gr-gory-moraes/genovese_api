import { Request, Response } from 'express';
import { CategoryService } from '../services/CreateCategoryService';

export class CategoryController {
    async create(request: Request, response: Response): Promise<Response> {
        const { name, typeIds } = request.body;

        const categoryService = new CategoryService();

        try {
            const category = await categoryService.create({ name, typeIds });
            return response.status(201).json(category);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async update(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;
        const { name, typeIds } = request.body;

        const categoryService = new CategoryService();

        try {
            const category = await categoryService.update(id, { name, typeIds });
            return response.status(200).json(category);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async list(request: Request, response: Response): Promise<Response> {
        const categoryService = new CategoryService();

        try {
            const categories = await categoryService.list();
            return response.status(200).json(categories);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async delete(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;

        const categoryService = new CategoryService();

        try {
            await categoryService.delete(id);
            return response.status(204).send();
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }
}

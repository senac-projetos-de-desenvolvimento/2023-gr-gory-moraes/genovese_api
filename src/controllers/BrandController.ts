import { Request, Response } from 'express';
import { BrandService } from '../services/CreateBrandService';

export class BrandController {
    async create(request: Request, response: Response): Promise<Response> {
        const { name, countryId } = request.body;

        const brandService = new BrandService();

        try {
            const brand = await brandService.create({ name, countryId });
            return response.status(201).json(brand);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async update(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;
        const { name, countryId } = request.body;

        const brandService = new BrandService();

        try {
            const brand = await brandService.update(id, { name, countryId });
            return response.status(200).json(brand);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async list(request: Request, response: Response): Promise<Response> {
        const brandService = new BrandService();

        try {
            const brands = await brandService.list();
            return response.status(200).json(brands);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async delete(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;

        const brandService = new BrandService();

        try {
            await brandService.delete(id);
            return response.status(204).send();
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }
}

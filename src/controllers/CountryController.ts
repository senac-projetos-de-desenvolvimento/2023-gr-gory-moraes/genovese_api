import { Request, Response } from 'express';
import { CountryService } from '../services/CreateCountryService';

export class CountryController {
    async create(request: Request, response: Response): Promise<Response> {
        const { name } = request.body;

        const countryService = new CountryService();

        try {
            const country = await countryService.create({ name });
            return response.status(201).json(country);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async update(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;
        const { name } = request.body;

        const countryService = new CountryService();

        try {
            const country = await countryService.update(id, { name });
            return response.status(200).json(country);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async list(request: Request, response: Response): Promise<Response> {
        const countryService = new CountryService();

        try {
            const countries = await countryService.list();
            return response.status(200).json(countries);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async delete(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;

        const countryService = new CountryService();

        try {
            await countryService.delete(id);
            return response.status(204).send();
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }
}

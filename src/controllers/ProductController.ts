import { Request, Response } from 'express';
import { ProductService } from '../services/CreateProductService';

export class ProductController {
    private productService: ProductService;

    constructor() {
        this.productService = new ProductService();
    }

    async create(request: Request, response: Response): Promise<Response> {
        const { name, description, image, userId, brandId, categoryId } = request.body;

        try {
            const product = await this.productService.create({ name, description, image, userId, brandId, categoryId });
            return response.status(201).json(product);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async list(request: Request, response: Response): Promise<Response> {
        try {
            const products = await this.productService.list();
            return response.status(200).json(products);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async update(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;
        const { name, description, image, userId, brandId, categoryId } = request.body;

        try {
            const product = await this.productService.update(id, { name, description, image, userId, brandId, categoryId });
            return response.status(200).json(product);
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async delete(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;

        try {
            await this.productService.delete(id);
            return response.status(204).send();
        } catch (error) {
            return response.status(400).json({ message: (error as Error).message });
        }
    }

    async getById(request: Request, response: Response): Promise<Response> {
        const { id } = request.params;

        try {
            const product = await this.productService.getById(id);
            return response.status(200).json(product);
        } catch (error) {
            return response.status(404).json({ message: (error as Error).message });
        }
    }
}

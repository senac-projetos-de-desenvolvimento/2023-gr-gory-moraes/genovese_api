import 'reflect-metadata';
import { DataSource } from 'typeorm';
import dotenv from 'dotenv';

dotenv.config(); // Carrega as variáveis de ambiente do arquivo .env
import User from './models/User';
import Product from './models/Product';
import Category from './models/Category';
import Brand from './models/Brand';
import Country from './models/Country';
import Type from './models/Type';
import SubType from './models/SubType';

export const AppDataSource = new DataSource({
  type: 'postgres',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),//O port só reconhece Number ou Undefined 
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  synchronize: true, // Usar apenas em desenvolvimento, desative em produção
  logging: true,
  entities: [User, Product, Category, Brand, Country, Type, SubType],
  migrations: ['src/migrations/*.ts'],
  subscribers: [],
});

import { Router } from 'express';
import { TypeController } from '../controllers/TypeController';

const typeRoutes = Router();
const typeController = new TypeController();

typeRoutes.post('/types', (request, response) => typeController.create(request, response));
typeRoutes.get('/types', (request, response) => typeController.list(request, response));
typeRoutes.put('/types/:id', (request, response) => typeController.update(request, response));
typeRoutes.delete('/types/:id', (request, response) => typeController.delete(request, response));

export default typeRoutes;
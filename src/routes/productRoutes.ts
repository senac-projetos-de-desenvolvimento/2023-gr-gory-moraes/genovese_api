import { Router } from 'express';
import { ProductController } from '../controllers/ProductController';

const productRoutes = Router();
const productController = new ProductController();

productRoutes.post('/products', (request, response) => productController.create(request, response));
productRoutes.get('/products', (request, response) => productController.list(request, response));
productRoutes.get('/products/:id', (request, response) => productController.getById(request, response));
productRoutes.put('/products/:id', (request, response) => productController.update(request, response));
productRoutes.delete('/products/:id', (request, response) => productController.delete(request, response));

export default productRoutes;
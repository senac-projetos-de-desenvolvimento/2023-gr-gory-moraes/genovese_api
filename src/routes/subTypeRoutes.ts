import { Router } from 'express';
import { SubTypeController } from '../controllers/SubTypeController';

const subTypeRoutes = Router();
const subtypeController = new SubTypeController();

subTypeRoutes.post('/subtypes', (request, response) => subtypeController.create(request, response));
subTypeRoutes.get('/subtypes', (request, response) => subtypeController.list(request, response));
subTypeRoutes.put('/subtypes/:id', (request, response) => subtypeController.update(request, response));
subTypeRoutes.delete('/subtypes/:id', (request, response) => subtypeController.delete(request, response));

export default subTypeRoutes;
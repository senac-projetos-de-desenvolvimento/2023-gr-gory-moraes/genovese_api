// src/routes/index.ts
import { Router } from 'express';
import brandRoutes from './brandRoutes';
import categoryRoutes from './categoryRoutes';
import countryRoutes from './countryRoutes';
import productRoutes from './productRoutes';
import subtypeRoutes from './subTypeRoutes';
import typeRoutes from './typeRoutes';
import userRoutes from './userRoutes';

const routes = Router();

brandRoutes.use(brandRoutes);
categoryRoutes.use(categoryRoutes);
countryRoutes.use(countryRoutes);
productRoutes.use(productRoutes);
subtypeRoutes.use(subtypeRoutes);
typeRoutes.use(typeRoutes);
userRoutes.use(userRoutes);

export default routes;

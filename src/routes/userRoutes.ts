import { Router } from 'express';
import { UserController } from '../controllers/UserController';

const userRoutes = Router();
const userController = new UserController();

userRoutes.post('/users', (request, response) => userController.create(request, response));
userRoutes.get('/users', (request, response) => userController.list(request, response));
userRoutes.put('/users/:id', (request, response) => userController.update(request, response));
userRoutes.delete('/users/:id', (request, response) => userController.delete(request, response));

export default userRoutes;
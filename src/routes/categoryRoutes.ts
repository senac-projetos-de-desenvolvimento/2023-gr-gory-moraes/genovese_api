import { Router } from 'express';
import { CountryController } from '../controllers/CountryController';

const categoryRoutes = Router();
const countryController = new CountryController();

categoryRoutes.post('/countries', (request, response) => countryController.create(request, response));
categoryRoutes.get('/countries', (request, response) => countryController.list(request, response));
categoryRoutes.put('/countries/:id', (request, response) => countryController.update(request, response));
categoryRoutes.delete('/countries/:id', (request, response) => countryController.delete(request, response));

export default categoryRoutes;
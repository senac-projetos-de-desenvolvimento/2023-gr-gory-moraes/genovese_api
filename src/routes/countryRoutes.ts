import { Router } from 'express';
import { CountryController } from '../controllers/CountryController';

const countryRoutes = Router();
const countryController = new CountryController();

countryRoutes.post('/categories', (request, response) => countryController.create(request, response));
countryRoutes.get('/categories', (request, response) => countryController.list(request, response));
countryRoutes.put('/categories/:id', (request, response) => countryController.update(request, response));
countryRoutes.delete('/categories/:id', (request, response) => countryController.delete(request, response));

export default countryRoutes;
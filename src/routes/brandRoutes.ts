import { Router } from 'express';
import { BrandController } from '../controllers/BrandController';

const brandRoutes = Router();
const brandController = new BrandController();

brandRoutes.post('/brands', (request, response) => brandController.create(request, response));
brandRoutes.get('/brands', (request, response) => brandController.list(request, response));
brandRoutes.put('/brands/:id', (request, response) => brandController.update(request, response));
brandRoutes.delete('/brands/:id', (request, response) => brandController.delete(request, response));

export default brandRoutes;
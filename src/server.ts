import 'reflect-metadata';
import express from 'express';
import { AppDataSource } from './data-source';
import userRoutes from './routes/userRoutes';
import productRoutes from './routes/productRoutes';
import categoryRoutes from './routes/categoryRoutes';
import brandRoutes from './routes/brandRoutes';
import countryRoutes from './routes/countryRoutes';
import typeRoutes from './routes/typeRoutes';
import subTypeRoutes from './routes/subTypeRoutes';
import dotenv from 'dotenv';
import { CountryService } from './services/CreateCountryService';
import { TypeService } from './services/CreateTypeService';
import { CategoryService } from './services/CreateCategoryService';
import { SubTypeService } from './services/CreateSubTypeService';
import { BrandService } from './services/CreateBrandService';

dotenv.config(); // Carrega as variáveis de ambiente do arquivo .env

const app = express();
app.use(express.json());

app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/categories', categoryRoutes);
app.use('/brands', brandRoutes);
app.use('/countries', countryRoutes);
app.use('/types', typeRoutes);
app.use('/subtypes', subTypeRoutes);

const PORT = process.env.PORT || 3001;

// Inicialize o DataSource
AppDataSource.initialize()
  .then(async () => {
    //Trás a inicialização dos paises no banco de dados    
    const countryService = new CountryService();
    await countryService.initializeCountries();
    
    //Trás a inicialização das categorias no banco de dados    
    const categoryService = new CategoryService();
    await categoryService.initializeCategories(); 

    //Trás a inicialização das marcas no banco de dados    
    const brandService = new BrandService();
    await brandService.initializeBrands(); 

    //Trás a inicialização dos tipos no banco de dados    
    const typeService = new TypeService();
    await typeService.initializeTypes();
    
    //Trás a inicialização dos subtipos no banco de dados    
    const subTypeService = new SubTypeService();
    await subTypeService.initializeSubTypes();

    console.log('Data Source has been initialized!');

    app.listen(PORT, () => {
      console.log(`🏃 Running Server on port ${PORT}`);
    });
  })
  .catch((err) => {
    console.error('Error during Data Source initialization:', err);
  });

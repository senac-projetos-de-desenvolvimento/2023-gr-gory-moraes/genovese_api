import { AppDataSource } from '../data-source';
import Type from '../models/Type';
import SubType from '../models/SubType';
import Category from '../models/Category';

type TypeRequest = {
    name: string;
    subTypeId: string;
    categoryIds?: string[];
};

export class TypeService {
    async create({ name, subTypeId, categoryIds }: TypeRequest): Promise<Type> {
        const typeRepo = AppDataSource.getRepository(Type);
        const subTypeRepo = AppDataSource.getRepository(SubType);
        const categoryRepo = AppDataSource.getRepository(Category);

        // Verifica se o subTipo existe
        const subType = await subTypeRepo.findOne({ where: { id: subTypeId } });
        if (!subType) {
            throw new Error("SubType not found/ SubTipo não encontrado");
        }

        // Busca as categorias
        let categories: Category[] = [];
        if (categoryIds && categoryIds.length > 0) {
            categories = await categoryRepo.findByIds(categoryIds);
            if (categories.length !== categoryIds.length) {
                throw new Error("One or more categories not found/ Uma ou mais categorias não encontradas");
            }
        }

        // Cria o novo tipo
        const type = typeRepo.create({ name, subType, categories });
        await typeRepo.save(type);

        return type;
    }

    async update(id: string, { name, subTypeId, categoryIds }: TypeRequest): Promise<Type> {
        const typeRepo = AppDataSource.getRepository(Type);
        const subTypeRepo = AppDataSource.getRepository(SubType);
        const categoryRepo = AppDataSource.getRepository(Category);

        // Verifica se o tipo existe
        const type = await typeRepo.findOne({ where: { id } });
        if (!type) {
            throw new Error("Type not found/ Tipo não encontrado");
        }

        // Atualiza os campos do tipo
        type.name = name;

        if (subTypeId) {
            const subType = await subTypeRepo.findOne({ where: { id: subTypeId } });
            if (!subType) {
                throw new Error("SubType not found/ SubTipo não encontrado");
            }
            type.subType = subType;
        }

        if (categoryIds && categoryIds.length > 0) {
            const categories = await categoryRepo.findByIds(categoryIds);
            if (categories.length !== categoryIds.length) {
                throw new Error("One or more categories not found/ Uma ou mais categorias não encontradas");
            }
            type.categories = categories;
        }

        await typeRepo.save(type);

        return type;
    }

    async list(): Promise<Type[]> {
        const typeRepo = AppDataSource.getRepository(Type);
        const types = await typeRepo.find({ relations: ["subType", "categories"] });
        return types;
    }

    async delete(id: string): Promise<void> {
        const typeRepo = AppDataSource.getRepository(Type);

        // Verifica se o tipo existe
        const type = await typeRepo.findOne({ where: { id } });
        if (!type) {
            throw new Error("Type not found/ Tipo não encontrado");
        }

        await typeRepo.remove(type);
    }

    async getById(id: string): Promise<Type | null> {
        const typeRepo = AppDataSource.getRepository(Type);
        return await typeRepo.findOne({ where: { id }, relations: ["subType", "categories"] });
    }


     // Método para inicializar tipos
     async initializeTypes(): Promise<void> {
        const typeRepo = AppDataSource.getRepository(Type);

        const types = [
            { name: 'Tinto' },
            { name: 'Branco' },
            { name: 'Rosé' },
            { name: 'Peixes' },
            { name: 'Crustácios' },
            { name: 'Pasta Fresca' },     
        ];

        for (const type of types) {
            const existingType = await typeRepo.findOne({ where: { name: type.name } });
            if (!existingType) {
                const newType = typeRepo.create(type);
                await typeRepo.save(newType);
            }
        }
    }
}

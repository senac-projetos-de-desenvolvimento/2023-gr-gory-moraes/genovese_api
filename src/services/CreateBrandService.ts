import { AppDataSource } from '../data-source';
import Brand from '../models/Brand';
import Country from '../models/Country';

type BrandRequest = {
    name: string;
    countryId: string;
};

export class BrandService {
    async create({ name, countryId }: BrandRequest): Promise<Brand | Error> {
        const brandRepo = AppDataSource.getRepository(Brand);
        const countryRepo = AppDataSource.getRepository(Country);

        // Verifica se a marca já existe
        const existingBrand = await brandRepo.findOne({ where: { name } });
        if (existingBrand) {
            throw new Error("Brand already exists/ Marca já existe");
        }

        // Verifica se o país existe
        const country = await countryRepo.findOne({ where: { id: countryId } });
        if (!country) {
            throw new Error("Country not found/ País não encontrado");
        }

        // Cria a nova marca
        const brand = brandRepo.create({
            name,
            country
        });

        await brandRepo.save(brand);

        return brand;
    }

    async update(id: string, { name, countryId }: BrandRequest): Promise<Brand | Error> {
        const brandRepo = AppDataSource.getRepository(Brand);
        const countryRepo = AppDataSource.getRepository(Country);

        // Verifica se a marca existe
        const brand = await brandRepo.findOne({ where: { id } });
        if (!brand) {
            throw new Error("Brand not found/ Marca não encontrada");
        }

        // Verifica se o país existe
        const country = await countryRepo.findOne({ where: { id: countryId } });
        if (!country) {
            throw new Error("Country not found/ País não encontrado");
        }

        // Atualiza os campos da marca
        brand.name = name;
        brand.country = country;

        await brandRepo.save(brand);

        return brand;
    }

    async list(): Promise<Brand[]> {
        const brandRepo = AppDataSource.getRepository(Brand);
        const brands = await brandRepo.find({ relations: ["country"] });
        return brands;
    }

    async delete(id: string): Promise<void> {
        const brandRepo = AppDataSource.getRepository(Brand);

        // Verifica se a marca existe
        const brand = await brandRepo.findOne({ where: { id } });
        if (!brand) {
            throw new Error("Brand not found/ Marca não encontrada");
        }

        await brandRepo.remove(brand);
    }


           // Método para inicializar marcas
           async initializeBrands(): Promise<void> {
            const brandRepo = AppDataSource.getRepository(Brand);
    
            const brands = [
                { name: 'Don Guerino' },
                { name: 'Las Perdices' },              
                { name: 'Millan' },              
                { name: 'Paganini' },              
                { name: 'LaPastina' },              
                { name: 'Frumar' },              
                { name: "D'italiani" },              
            ];
            
            for (const brand of brands) {
                const existingBrand = await brandRepo.findOne({ where: { name: brand.name } });
                if (!existingBrand) {
                    const newBrand = brandRepo.create(brand);
                    await brandRepo.save(newBrand);
                }
            }
        }
}

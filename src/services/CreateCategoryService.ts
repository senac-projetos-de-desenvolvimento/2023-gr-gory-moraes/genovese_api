import { AppDataSource } from '../data-source';
import Category from '../models/Category';
import Type from '../models/Type';

type CategoryRequest = {
    name: string;
    typeIds?: string[];
};

export class CategoryService {
    async create({ name, typeIds }: CategoryRequest): Promise<Category> {
        const categoryRepo = AppDataSource.getRepository(Category);
        const typeRepo = AppDataSource.getRepository(Type);

        // Verifica se a categoria já existe
        const existingCategory = await categoryRepo.findOne({ where: { name } });
        if (existingCategory) {
            throw new Error("Category already exists/ Categoria já existe");
        }

        // Busca os tipos
        let types: Type[] = [];
        if (typeIds && typeIds.length > 0) {
            types = await typeRepo.findByIds(typeIds);
            if (types.length !== typeIds.length) {
                throw new Error("One or more types not found/ Um ou mais tipos não encontrados");
            }
        }

        // Cria a nova categoria
        const category = categoryRepo.create({ name, types });
        await categoryRepo.save(category);

        return category;
    }

    async update(id: string, { name, typeIds }: CategoryRequest): Promise<Category> {
        const categoryRepo = AppDataSource.getRepository(Category);
        const typeRepo = AppDataSource.getRepository(Type);

        // Verifica se a categoria existe
        const category = await categoryRepo.findOne({ where: { id } });
        if (!category) {
            throw new Error("Category not found/ Categoria não encontrada");
        }

        // Atualiza os campos da categoria
        category.name = name;

        if (typeIds && typeIds.length > 0) {
            const types = await typeRepo.findByIds(typeIds);
            if (types.length !== typeIds.length) {
                throw new Error("One or more types not found/ Um ou mais tipos não encontrados");
            }
            category.types = types;
        }

        await categoryRepo.save(category);

        return category;
    }

    async list(): Promise<Category[]> {
        const categoryRepo = AppDataSource.getRepository(Category);
        const categories = await categoryRepo.find({ relations: ["types"] });
        return categories;
    }

    async delete(id: string): Promise<void> {
        const categoryRepo = AppDataSource.getRepository(Category);

        // Verifica se a categoria existe
        const category = await categoryRepo.findOne({ where: { id } });
        if (!category) {
            throw new Error("Category not found/ Categoria não encontrada");
        }

        await categoryRepo.remove(category);
    }

     // Método para inicializar categorias
     async initializeCategories(): Promise<void> {
        const categoryRepo = AppDataSource.getRepository(Category);

        const categories = [
            { name: 'Vinhos' },
            { name: 'Destilados' },
            { name: 'Pescados' },
            { name: 'Massas' },
            { name: 'Queijos' },
            { name: 'Delicatessen' },     
        ];

        for (const category of categories) {
            const existingCategory = await categoryRepo.findOne({ where: { name: category.name } });
            if (!existingCategory) {
                const newCategory = categoryRepo.create(category);
                await categoryRepo.save(newCategory);
            }
        }
    }
}

import { AppDataSource } from '../data-source';
import bcrypt from 'bcrypt';
import User from '../models/User';

type UserRequest = {
    name: string;
    email: string;
    password: string;
};

export class UserService {
    async create({ name, email, password }: UserRequest): Promise<User | Error> {
        const userRepo = AppDataSource.getRepository(User);

        const existingUser = await userRepo.findOne({ where: { email } });
        if (existingUser) {
            return new Error("User already exists/ Usuário já existe");
        }

        const hashedPassword = await bcrypt.hash(password, 10);

        const user = userRepo.create({ name, email, password: hashedPassword });
        await userRepo.save(user);

        return user;
    }

    async update(id: string, { name, email, password }: UserRequest): Promise<User | Error> {
        const userRepo = AppDataSource.getRepository(User);

        const user = await userRepo.findOne({ where: { id } });
        if (!user) {
            return new Error("User not found/ Usuário não encontrado");
        }

        user.name = name;
        user.email = email;
        if (password) {
            user.password = await bcrypt.hash(password, 10);
        }

        await userRepo.save(user);

        return user;
    }

    async list(): Promise<User[]> {
        const userRepo = AppDataSource.getRepository(User);
        return await userRepo.find();
    }

    async delete(id: string): Promise<void | Error> {
        const userRepo = AppDataSource.getRepository(User);

        const user = await userRepo.findOne({ where: { id } });
        if (!user) {
            return new Error("User not found/ Usuário não encontrado");
        }

        await userRepo.remove(user);
    }
}

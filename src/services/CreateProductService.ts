import { AppDataSource } from '../data-source';
import Product from '../models/Product';
import User from '../models/User';
import Brand from '../models/Brand';
import Category from '../models/Category';

type ProductRequest = {
    name: string;
    description: string;
    image: string;
    userId: string;
    brandId: string;
    categoryId: string;
};

export class ProductService {
    async create({ name, description, image, userId, brandId, categoryId }: ProductRequest): Promise<Product | Error> {
        const productRepo = AppDataSource.getRepository(Product);
        const userRepo = AppDataSource.getRepository(User);
        const brandRepo = AppDataSource.getRepository(Brand);
        const categoryRepo = AppDataSource.getRepository(Category);

        const user = await userRepo.findOne({ where: { id: userId } });
        if (!user) {
            return new Error("User not found/ Usuário não encontrado");
        }

        const brand = await brandRepo.findOne({ where: { id: brandId } });
        if (!brand) {
            return new Error("Brand not found/ Marca não encontrada");
        }

        const category = await categoryRepo.findOne({ where: { id: categoryId } });
        if (!category) {
            return new Error("Category not found/ Categoria não encontrada");
        }

        const product = productRepo.create({
            name,
            description,
            image,
            user,
            brand,
            category,
        });

        await productRepo.save(product);

        return product;
    }

    async update(id: string, { name, description, image, userId, brandId, categoryId }: ProductRequest): Promise<Product | Error> {
        const productRepo = AppDataSource.getRepository(Product);
        const userRepo = AppDataSource.getRepository(User);
        const brandRepo = AppDataSource.getRepository(Brand);
        const categoryRepo = AppDataSource.getRepository(Category);

        const product = await productRepo.findOne({ where: { id } });
        if (!product) {
            return new Error("Product not found/ Produto não encontrado");
        }

        const user = await userRepo.findOne({ where: { id: userId } });
        if (!user) {
            return new Error("User not found/ Usuário não encontrado");
        }

        const brand = await brandRepo.findOne({ where: { id: brandId } });
        if (!brand) {
            return new Error("Brand not found/ Marca não encontrada");
        }

        const category = await categoryRepo.findOne({ where: { id: categoryId } });
        if (!category) {
            return new Error("Category not found/ Categoria não encontrada");
        }

        product.name = name;
        product.description = description;
        product.image = image;
        product.user = user;
        product.brand = brand;
        product.category = category;

        await productRepo.save(product);

        return product;
    }

    async list(): Promise<Product[]> {
        const productRepo = AppDataSource.getRepository(Product);
        return await productRepo.find({ relations: ["user", "brand", "category"] });
    }

    async delete(id: string): Promise<void | Error> {
        const productRepo = AppDataSource.getRepository(Product);

        const product = await productRepo.findOne({ where: { id } });
        if (!product) {
            return new Error("Product not found/ Produto não encontrado");
        }

        await productRepo.remove(product);
    }

    async getById(id: string): Promise<Product | null> {
        const productRepo = AppDataSource.getRepository(Product);
        return await productRepo.findOne({ where: { id } });
    }
}

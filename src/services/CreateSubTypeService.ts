import { AppDataSource } from '../data-source';
import SubType from '../models/SubType';
import Type from '../models/Type';

type SubTypeRequest = {
    name: string;
    typeId: string;
};

export class SubTypeService {
    async create({ name, typeId }: SubTypeRequest): Promise<SubType | Error> {
        const subTypeRepo = AppDataSource.getRepository(SubType);
        const typeRepo = AppDataSource.getRepository(Type);

        const type = await typeRepo.findOne({ where: { id: typeId } });
        if (!type) {
            return new Error("Type not found/ Tipo não encontrado");
        }

        const subType = subTypeRepo.create({ name, type });

        await subTypeRepo.save(subType);

        return subType;
    }

    async update(id: string, { name, typeId }: SubTypeRequest): Promise<SubType | Error> {
        const subTypeRepo = AppDataSource.getRepository(SubType);
        const typeRepo = AppDataSource.getRepository(Type);

        const subType = await subTypeRepo.findOne({ where: { id } });
        if (!subType) {
            return new Error("SubType not found/ SubTipo não encontrado");
        }

        const type = await typeRepo.findOne({ where: { id: typeId } });
        if (!type) {
            return new Error("Type not found/ Tipo não encontrado");
        }

        subType.name = name;
        subType.type = type;

        await subTypeRepo.save(subType);

        return subType;
    }

    async list(): Promise<SubType[]> {
        const subTypeRepo = AppDataSource.getRepository(SubType);
        return await subTypeRepo.find({ relations: ["type"] });
    }

    async delete(id: string): Promise<void | Error> {
        const subTypeRepo = AppDataSource.getRepository(SubType);

        const subType = await subTypeRepo.findOne({ where: { id } });
        if (!subType) {
            return new Error("SubType not found/ SubTipo não encontrado");
        }

        await subTypeRepo.remove(subType);
    }


        // Método para inicializar subtipos
        async initializeSubTypes(): Promise<void> {
            const subTypeRepo = AppDataSource.getRepository(SubType);
    
            const subTypes = [
                { name: 'Cabernet Sauvignon' },
                { name: 'Merlot' },
                { name: 'Carmenere' },
                { name: 'Malbec' },
                { name: 'Pinot Noir' },
                { name: 'Sauvignon Blanc' },
                { name: 'Riesling' },
                { name: 'Chardonnay' },
                { name: 'Salmão' },
                { name: 'Bacalhau' },
                { name: 'Linguado' },
                { name: 'Mexilhões' },
                { name: 'Camarão' },
                { name: 'Anéis de lulas' },
                { name: 'Capeletti' },
                { name: 'Ravioli' },
                { name: 'Rondelli' },
                { name: 'Sorrentino' },
                { name: 'Vodka' },     
                { name: 'Cachaça' },     
                { name: 'Espaguete' },     
                { name: 'Conchione' },     
                { name: 'Paparfelli' },     
                { name: 'Fetuchine' },   
            ];
            
            for (const subType of subTypes) {
                const existingSubType = await subTypeRepo.findOne({ where: { name: subType.name } });
                if (!existingSubType) {
                    const newSubType = subTypeRepo.create(subType);
                    await subTypeRepo.save(newSubType);
                }
            }
        }
}

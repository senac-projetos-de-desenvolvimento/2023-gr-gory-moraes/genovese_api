import { AppDataSource } from '../data-source';
import Country from '../models/Country';

type CountryRequest = {
    name: string;
};

export class CountryService {

    // Método de criação
    async create({ name }: CountryRequest): Promise<Country | Error> {
        const countryRepo = AppDataSource.getRepository(Country);

        const existingCountry = await countryRepo.findOne({ where: { name } });
        if (existingCountry) {
            return new Error("Country already exists/ País já existe");
        }

        const country = countryRepo.create({ name });
        await countryRepo.save(country);

        return country;
    }

    // Método de altualização
    async update(id: string, { name }: CountryRequest): Promise<Country | Error> {
        const countryRepo = AppDataSource.getRepository(Country);

        const country = await countryRepo.findOne({ where: { id } });
        if (!country) {
            return new Error("Country not found/ País não encontrado");
        }

        country.name = name;

        await countryRepo.save(country);

        return country;
    }

    async list(): Promise<Country[]> {
        const countryRepo = AppDataSource.getRepository(Country);
        return await countryRepo.find({ relations: ["brands"] });
    }

    // Método de exclusão
    async delete(id: string): Promise<void | Error> {
        const countryRepo = AppDataSource.getRepository(Country);

        const country = await countryRepo.findOne({ where: { id } });
        if (!country) {
            return new Error("Country not found/ País não encontrado");
        }

        await countryRepo.remove(country);
    }


    // Método para inicializar países
    async initializeCountries(): Promise<void> {
        const countryRepo = AppDataSource.getRepository(Country);

        const countries = [
            { name: 'África do Sul' },
            { name: 'Alemanha' },
            { name: 'Argentina' },
            { name: 'Austrália' },
            { name: 'Áustria' },
            { name: 'Azerbaijão' },
            { name: 'Bélgica' },
            { name: 'Bolívia' },
            { name: 'Brasil' },
            { name: 'Bulgária' },
            { name: 'Canadá' },
            { name: 'Chile' },
            { name: 'China' },
            { name: 'Colômbia' },
            { name: 'Cuba' },
            { name: 'Dinamarca' },
            { name: 'Egito' },
            { name: 'Equador' },
            { name: 'Espanha' },
            { name: 'Estados Unidos' },
            { name: 'Finlândia' },
            { name: 'França' },
            { name: 'Grécia' },
            { name: 'Hungria' },
            { name: 'Índia' },
            { name: 'Irã' },
            { name: 'Israel' },
            { name: 'Itália' },
            { name: 'Jamaica' },
            { name: 'Japão' },
            { name: 'Líbano' },
            { name: 'México' },
            { name: 'Moldávia' },
            { name: 'Noruega' },
            { name: 'Nova Zelândia' },
            { name: 'Paraguai' },
            { name: 'Peru' },
            { name: 'Polônia' },
            { name: 'Portugal' },
            { name: 'Reino Unido' },
            { name: 'Romênia' },
            { name: 'Rússia' },
            { name: 'Síria' },
            { name: 'Suécia' },
            { name: 'Suíça' },
            { name: 'Turquia' },
            { name: 'Tuvalu' },
            { name: 'Ucrânia' },
            { name: 'Uruguai' },
            { name: 'Uzbequistão' },
            { name: 'Venezuela' },
            { name: 'Vietnã' }, 
        ];

        for (const country of countries) {
            const existingCountry = await countryRepo.findOne({ where: { name: country.name } });
            if (!existingCountry) {
                const newCountry = countryRepo.create(country);
                await countryRepo.save(newCountry);
            }
        }
    }
}

import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    ManyToMany,
    OneToMany,
    JoinTable
} from 'typeorm';
import Type from './Type';
import Product from './Product';

@Entity()
export default class Category {
    @PrimaryGeneratedColumn('uuid')
    id: String;

    @Column()
    name: String;

    @ManyToMany(() => Type, (type) => type.categories)
    @JoinTable() // Necessário no lado proprietário do relacionamento
    types: Type[];

    @OneToMany(() => Product, (product) => product.category)
    products: Product[];

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;
}
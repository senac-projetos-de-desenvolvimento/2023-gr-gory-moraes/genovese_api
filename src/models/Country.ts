import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    OneToMany,
} from 'typeorm';
import Brand from './Brand';

@Entity()
export default class Country {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    //Relacionamento um para um entre países(countries) e marcas(brand)
    @OneToMany(() => Brand, (brand) => brand.country)
    brands: Brand[];

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;
}
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    OneToOne,
    JoinColumn,
} from 'typeorm';
import Type from './Type';

@Entity()// Se o nome tabela for diferente, referencio dentro dos parenteses da Entity('subType') senão pode ficar ().  
export default class SubType {
    @PrimaryGeneratedColumn('uuid')
    id: String;

    @Column()
    name: String;

    @OneToOne(() => Type, (type) => type.subType) // Define o relacionamento
    @JoinColumn({ name: "type_id" }) // Especifica a coluna de chave estrangeira
    type: Type

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;
}
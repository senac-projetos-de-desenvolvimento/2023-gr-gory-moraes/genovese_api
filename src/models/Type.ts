import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    OneToOne,
    ManyToMany,
    JoinColumn,
} from 'typeorm';
import SubType from './SubType';
import Category from './Category';

@Entity()// Se o nome tabela for diferente, referencio dentro dos parenteses da Entity('type') senão pode ficar ().  
export default class Type {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    @OneToOne(() => SubType, (subType) => subType.type) // Define o relacionamento
    @JoinColumn({ name: "subType_id" }) // Especifica a coluna de chave estrangeira
    subType: SubType;

    @ManyToMany(() => Category, (category) => category.types)
    categories: Category[];

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;
}